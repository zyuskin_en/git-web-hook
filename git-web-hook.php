<?php
/*
	Скрипт для оброботки web hooks с git
	Автор: Егор Зюскин (zyuskin_en@me.com)
	URL: https://gitlab.com/zyuskin_en/git-web-hook
*/

if (!file_exists('git-web-hook-conf.php'))
	die();

$settings = include 'git-web-hook-conf.php';


$json = trim(@file_get_contents('php://input'));
_log($json, 'json');

/*  === Начнем проверки === */

if (!function_exists('json_decode'))
	throw new Exception();

/*Проверяем что запрос нужного типа*/
if (isset($_SERVER['CONTENT_TYPE']) && $_SERVER['CONTENT_TYPE'] != 'application/json')
	_die('Не верный тип контента: '.$_SERVER['CONTENT_TYPE'], 'wrong_content_type');

/* Ограничеваем доступ к хукам по ip*/
if (isset($settings['allow_request_from']) && isset($_SERVER['REMOTE_ADDR']))
{
	if (is_array($settings['allow_request_from']) && ! in_array($_SERVER['REMOTE_ADDR'], $settings['allow_request_from']))
		_die('IP '.$_SERVER['REMOTE_ADDR'].' не найден в списке разрешенных IP адресов', 'check_ip');
	elseif ($settings['allow_request_from'] != '*' && $_SERVER['REMOTE_ADDR'] != $settings['allow_request_from'])	
		_die('IP '.$_SERVER['REMOTE_ADDR'].' не разрешен', 'check_ip');
}

if ($json == '')
	_die('JSON запрос пустой', 'empty_json');

$json = json_decode($json, true);
/*Если есть ref значит это gitlab*/
if (isset($json['ref']))
{
	$author = $json['user_name'];
	$repo   = $json['repository']['url'];
	$email  = $json['commits'][$json['total_commits_count']-1]['author']['email'];
	list($tmp, $head, $branch) = explode('/', $json['ref']);
}
/*Пока выпубим остальные запросы*/
else
{
	_die();
}

if (!isset($settings['repos'][$repo]))
	_die('Репозиторий '.$repo.' не найден в настройках.', 'repo_not_found');

$repo_settings = $settings['repos'][$repo];

if (isset($repo_settings['allow_request_from']) && !_check($repo_settings['allow_request_from'], $_SERVER['REMOTE_ADDR']))
	_die('IP '.$_SERVER['REMOTE_ADDR'].' не разрешен для репозитория '.$repo, 'check_ip');

if (!isset($repo_settings['branches'][$branch]))
	_die('Не найдены настройки для branch '.$branch.' для репозитория '.$repo, 'branche_not_found');

$branch_settings = $repo_settings['branches'][$branch];

if (isset($branch_settings['allow_authors']) && (!_check($branch_settings['allow_authors'], $author)))
	_die('Автор '.$author.' не разрешен для бранча '.$branch.' в репозитории '.$repo, 'author_not_allow');
	
if (!isset($branch_settings['actions']) || !is_array($branch_settings['actions']) || count($branch_settings['actions']) == 0)
	_die('Не найдено действий для бранча '.$branch.' в репозитории '.$repo, 'actions_not_found');

$message = "";
	
$dirs = array_keys($branch_settings['actions']);
foreach($dirs as $dir)
{
	chdir($dir);
	if (is_array($branch_settings['actions'][$dir]))
	{
		foreach($branch_settings['actions'][$dir] as $action)
		{
			_log("Выполняем ".$action." в ".$dir, "action");
			$out = array();
			exec($action, $out);
			$message .= $action . "\n===========================\n" . implode("\n", $out);
			_log("Результат выполнения ".implode("\n", $out), 'action_result');
		}
	}
}

if (isset($repo_settings['email_send']) && $repo_settings['email_send'])
{
	if (trim($email) != "")
	{
		_log('Отправляем почту на '.$email, 'email');
		mail($email, php_uname('n') . ' git-web-hook-'.$repo.'-'.$branch, $message);
	}
	if (isset($repo_settings['owner_email']) && trim($repo_settings['owner_email']) != "" && trim($repo_settings['owner_email']) != $email)
	{
		_log('Отправляем почту на '.$repo_settings['owner_email'], 'email');
		mail($repo_settings['owner_email'], php_uname('n').' git-web-hook-'.$repo.'-'.$branch, $message);
	}
}
	
function _die($text = '', $path = '')
{
	if ($text)
		_log($text, $path);
	header('HTTP/1.0 404 Not Found');
	echo "<h1>404 Not Found</h1>";
	echo "The page that you have requested could not be found.";
    die();
}

function _log($text, $path = "")
{
	global $settings;
	if ((!isset($settings['logs']['filter'][$path]) || !$settings['logs']['filter'][$path]) && $path != '' && !$settings['logs']['debug'])
		return;
	$file = fopen($settings['logs']['file'], 'a');
	fwrite($file, $_SERVER['REMOTE_ADDR'].' '.$_SERVER['HTTP_HOST'].' '.date('d.m.Y H:i').' '.$text."\n");
	fclose($file);
}

function _check($where, $that, $allow_asterisk = true)
{
	if (is_array($where) && in_array($that, $where))
		return true;
	elseif ($where == '*' || trim($where) == trim($that))
		return true;
	else
		return false;
}

?>